# MVVM Light Learning Project

This project is on implementing basic and few advanced features of MVVM Light toolkit by Galasoft.

### Prerequisites

Nuget package MVVM Light.

### Installing

Clone and go.

### Topics Covered

* Basics	
  *   Installing MVVM Light Package	
  *  Boilerplate Code and Registering ViewModel classes	
  *  Commands	
     *  Databinding Commands to UI Elements not having Command Property	
  * Mvvm Light Views	
  * Registering Services	
  * Sharing Data Between Views and ViewModels	
* Advanced Topics	
  * Opening New Windows/Dialogs from ViewModels	
    * Using Messaging	
    * Using DialogService
      * MVVM Light Default IDialogService
      * Custom DialogService
