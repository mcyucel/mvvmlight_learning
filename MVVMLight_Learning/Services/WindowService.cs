﻿using GalaSoft.MvvmLight.Ioc;
using MVVMLight_Learning.ViewModel;

namespace MVVMLight_Learning.Services
{
    public class WindowService : IWindowService
    {
        public void OpenAddWindowDifferentVM()
        {
            var vm = SimpleIoc.Default.GetInstance<AddEmployeeViewModel>();
            var window = new AddEmployee() { DataContext = vm };
            window.Show();
        }

        public void OpenAddWindowSameVM()
        {
            var window = new AddEmployee();
            window.Show();
        }
    }
}
