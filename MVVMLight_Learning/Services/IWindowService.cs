﻿namespace MVVMLight_Learning.Services
{
    public interface IWindowService
    {
        void OpenAddWindowSameVM();
        void OpenAddWindowDifferentVM();
    }
}
