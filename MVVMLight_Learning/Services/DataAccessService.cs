﻿using MVVMLight_Learning.Model;
using System;
using System.Collections.Generic;

namespace MVVMLight_Learning.Services
{
    public class DataAccessService : IDataAccessService
    {
        public DataAccessService()
        {
            // any EF context initialization here
        }
        public int CreateEmployee(EmployeeInfo emp)
        {
            // persist the employee to the db and return its id
            return new Random().Next(0, 100);
        }

        public List<EmployeeInfo> GetEmployees()
        {
            // read the list of employees from db
            return new List<EmployeeInfo>
            {
                new EmployeeInfo { EmpNo = 1, EmpName = "Employee 1", Salary = 1000, DeptName = "A1", Designation = "D1"},
                new EmployeeInfo { EmpNo = 2, EmpName = "Employee 2", Salary = 2000, DeptName = "A2", Designation = "D2"},
                new EmployeeInfo { EmpNo = 3, EmpName = "Employee 3", Salary = 3000, DeptName = "A3", Designation = "D3"},
                new EmployeeInfo { EmpNo = 4, EmpName = "Employee 4", Salary = 4000, DeptName = "A4", Designation = "D4"},
                new EmployeeInfo { EmpNo = 5, EmpName = "Employee 5", Salary = 5000, DeptName = "A5", Designation = "D5"},
                new EmployeeInfo { EmpNo = 6, EmpName = "Employee 6", Salary = 6000, DeptName = "A6", Designation = "D6"},
                new EmployeeInfo { EmpNo = 7, EmpName = "Employee 7", Salary = 7000, DeptName = "A7", Designation = "D7"},
                new EmployeeInfo { EmpNo = 8, EmpName = "Employee 8", Salary = 8000, DeptName = "A8", Designation = "D8"},
                new EmployeeInfo { EmpNo = 9, EmpName = "Employee 9", Salary = 9000, DeptName = "A9", Designation = "D9"},
                new EmployeeInfo { EmpNo = 10, EmpName = "Employee 10", Salary = 10000, DeptName = "A10", Designation = "D10"}
            };
        }
    }
}
