﻿using MVVMLight_Learning.Model;
using System.Collections.Generic;

namespace MVVMLight_Learning.Services
{
    public interface IDataAccessService
    {
        List<EmployeeInfo> GetEmployees();
        int CreateEmployee(EmployeeInfo emp);
    }
}
