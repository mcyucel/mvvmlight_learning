using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using MVVMLight_Learning.MessageInfrastructure;
using MVVMLight_Learning.Model;
using MVVMLight_Learning.Services;
using System;
using System.Collections.ObjectModel;

namespace MVVMLight_Learning.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        ObservableCollection<EmployeeInfo> employees;
        EmployeeInfo employee;
        readonly IDataAccessService serviceProxy;
        readonly IDialogService dialogProxy;
        readonly IWindowService windowProxy;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataAccessService _proxy, IDialogService _dialogProxy, IWindowService _windowProxy)
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            ///
            serviceProxy = _proxy;
            dialogProxy = _dialogProxy;
            windowProxy = _windowProxy;
            Employees = new ObservableCollection<EmployeeInfo>();
            ReadAllCommand = new RelayCommand(GetEmployees);
            SaveCommand = new RelayCommand<EmployeeInfo>(Save);
            SendEmployeeCommand = new RelayCommand<EmployeeInfo>(SendEmployeeInfo);
            OpenWindowByMessagingSameVMCommand = new RelayCommand(OpenWindowByMessagingSameVM);
            OpenWindowByMessagingDifferentVMCommand = new RelayCommand(OpenWindowByMessagingDifferentVM);
            ShowMessageBoxByServiceCommand = new RelayCommand(ShowMessageBoxByService);
            OpenWindowByServiceSameVMCommand = new RelayCommand(OpenWindowByServiceSameVM);
            OpenWindowByServiceDifferentVMCommand = new RelayCommand(OpenWindowByServiceDifferentVM);
            Employee = new EmployeeInfo();

            // listen to the messages of the different view model add employee windows
            Messenger.Default.Register<SaveEmployeeMessage>(this, (message) => { Employees.Add(message.NewEmployee); });
        }

        private void OpenWindowByServiceDifferentVM()
        {
            windowProxy.OpenAddWindowDifferentVM();
        }

        private void OpenWindowByServiceSameVM()
        {
            windowProxy.OpenAddWindowSameVM();
        }

        private async void ShowMessageBoxByService()
        {
            await dialogProxy.ShowMessage("Test message", "Test title");
        }

        private void OpenWindowByMessagingDifferentVM()
        {
            // we need to listen to the result message of the below window because the two windows have separate view models
            Messenger.Default.Send(new OpenWindowMessage { NewWindowType = NewWindowType.AddEmployeeDifferentViewModel });
        }

        private void OpenWindowByMessagingSameVM()
        {
            // we don't need to listen to the result of the below window because it processes over the same VM instance; this one. Thus Save command is run over this instance
            Messenger.Default.Send(new OpenWindowMessage { NewWindowType = NewWindowType.AddEmployeeSameViewModel });
        }

        public RelayCommand ReadAllCommand { get; set; }
        public RelayCommand ShowMessageBoxByServiceCommand { get; set; }
        public RelayCommand<EmployeeInfo> SaveCommand { get; set; }
        public RelayCommand<EmployeeInfo> SendEmployeeCommand { get; set; }
        public RelayCommand OpenWindowByMessagingSameVMCommand { get; set; }
        public RelayCommand OpenWindowByMessagingDifferentVMCommand { get; set; }
        public RelayCommand OpenWindowByServiceSameVMCommand { get; set; }
        public RelayCommand OpenWindowByServiceDifferentVMCommand { get; set; }
        public ObservableCollection<EmployeeInfo> Employees { get => employees; set { employees = value; RaisePropertyChanged(); } }

        public EmployeeInfo Employee { get => employee; set { employee = value; RaisePropertyChanged(); } }

        void GetEmployees()
        {
            Employees.Clear();
            serviceProxy.GetEmployees().ForEach(q => Employees.Add(q));
        }

        void Save(EmployeeInfo employeeInfo)
        {
            Employees.Add(Employee);
        }

        void SendEmployeeInfo(EmployeeInfo employee)
        {
            
            if (employee != null)
            {
                Messenger.Default.Send(new DetailsPayload { EmployeeInfo = employee });
            }
        }
    }
}