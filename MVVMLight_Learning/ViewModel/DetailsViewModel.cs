﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using MVVMLight_Learning.MessageInfrastructure;
using MVVMLight_Learning.Model;

namespace MVVMLight_Learning.ViewModel
{
    public class DetailsViewModel : ViewModelBase
    {
        EmployeeInfo employee;


        public DetailsViewModel()
        {
            Messenger.Default.Register<DetailsPayload>(this, (payload) => { Employee = payload.EmployeeInfo; });
        }

        public EmployeeInfo Employee { get => employee; set { employee = value; RaisePropertyChanged(); } }

    }
}
