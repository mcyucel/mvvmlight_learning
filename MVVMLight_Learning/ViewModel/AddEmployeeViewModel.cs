﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MVVMLight_Learning.MessageInfrastructure;
using MVVMLight_Learning.Model;

namespace MVVMLight_Learning.ViewModel
{
    public class AddEmployeeViewModel : ViewModelBase
    {
        public EmployeeInfo Employee { get; set; }
        public RelayCommand SaveCommand { get; set; }

        public AddEmployeeViewModel()
        {
            Employee = new EmployeeInfo();
            SaveCommand = new RelayCommand(Save);
        }

        private void Save()
        {
            Messenger.Default.Send(new SaveEmployeeMessage { NewEmployee = Employee });
        }
    }
}
