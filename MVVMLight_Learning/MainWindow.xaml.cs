﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using MVVMLight_Learning.MessageInfrastructure;
using MVVMLight_Learning.ViewModel;
using System.Windows;

namespace MVVMLight_Learning
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //AddEmployee w = new AddEmployee();
            //w.Show();
            DetailsWindow d = new DetailsWindow();
            d.Show();
            // Register to messaging service for opening new windows using Messaging
            Messenger.Default.Register<OpenWindowMessage>(this, OpenNewWindow);
        }

        private void OpenNewWindow(OpenWindowMessage messsage)
        {
            switch (messsage.NewWindowType)
            {
                case NewWindowType.AddEmployeeSameViewModel:
                    {
                        // we don't need to set datacontext manually because it is defined in the XAML of the addEmployee window
                        var addWindow = new AddEmployee();
                        addWindow.Show();
                    }
                    break;
                case NewWindowType.AddEmployeeDifferentViewModel:
                    {
                        // we need to define the datacontext if we want to keep the auto data context property defined in the XAML of the addEmployee window
                        // if not, we can just use auto data context generation of MVVM Light
                        var newVM = SimpleIoc.Default.GetInstance<AddEmployeeViewModel>();
                        // the following assignment will overwrite XAML defined datacontext
                        var addWindow = new AddEmployee() { DataContext = newVM };
                        addWindow.Show();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
