﻿namespace MVVMLight_Learning.Model
{
    public class EmployeeInfo
    {
        public int EmpNo { get; set; }
        public string EmpName { get; set; }
        public double Salary { get; set; }
        public string DeptName { get; set; }
        public string Designation { get; set; }
    }
}
