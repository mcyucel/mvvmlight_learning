﻿using MVVMLight_Learning.Model;

namespace MVVMLight_Learning.MessageInfrastructure
{
    public class DetailsPayload
    {
        public EmployeeInfo EmployeeInfo { get; set; }
    }
}
