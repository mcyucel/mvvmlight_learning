﻿namespace MVVMLight_Learning.MessageInfrastructure
{
    public enum NewWindowType
    {
        AddEmployeeSameViewModel,
        AddEmployeeDifferentViewModel
    }

    public class OpenWindowMessage
    {
        public NewWindowType NewWindowType { get; set; }
    }
}
