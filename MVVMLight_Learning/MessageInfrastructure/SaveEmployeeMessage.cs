﻿using MVVMLight_Learning.Model;

namespace MVVMLight_Learning.MessageInfrastructure
{
    public class SaveEmployeeMessage
    {
        public EmployeeInfo NewEmployee { get; set; }
    }
}
